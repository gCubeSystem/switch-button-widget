
# Changelog for workspace

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.3.0] - 2023-07-25

- Moved to gwt 2.10.0
- Removed the `gwtquery` dependency [#25436]

## [v1.2.0] - 2022-10-28

- [#24053] Fixing issue 
- Moved to maven-portal-bom 3.6.4
- Moved to gwtquery 1.5-beta1
- Moved to gwt.version 2.8.2

## [v1.1.0] - 2020-03-21

Ported to git

## [v1.0.0] - 2013-10-21

First release 
